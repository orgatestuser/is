﻿
using System;
using Orgadata.Framework.Translation;
using Shared.Translations;

namespace AdminClient.Translations
{
    public class AdminTranslations : SharedTranslations
    {
        /// <summary>
        /// "Werkstattleiter"
        /// </summary>
        public static String ProgramName
        {
            get { return GlobalTranslation.Instance.Tx("Werkstattleiter",600,181); }
        }

        /// <summary>
        /// "Projektverwaltung"
        /// </summary>
        public static String BatchConfigurationTitle
        {
            get { return GlobalTranslation.Instance.Tx("Projektverwaltung",600,182); }
        }

        /// <summary>
        /// "Inaktive Projekte"
        /// </summary>
        public static String InActiveBatches
        {
            get { return GlobalTranslation.Instance.Tx("Inaktive Projekte", 600, 123); }
        }

        /// <summary>
        /// "Aktive Projekte"
        /// </summary>
        public static String ActiveBatches
        {
            get { return GlobalTranslation.Instance.Tx("Aktive Projekte", 600, 124); }
        }

        /// <summary>
        /// "Abgeschlossene Projekte"
        /// </summary>
        public static String CompletedBatches
        {
            get { return GlobalTranslation.Instance.Tx("Abgeschlossene Projekte",600,183); }
        }

        /// <summary>
        /// "Ohne"
        /// </summary>
        public static String Ohne
        {
            get { return GlobalTranslation.Instance.Tx("Ohne", 600, 129); }
        }

        /// <summary>
        /// "WPK-Sets bearbeiten"
        /// </summary>
        public static String EditFpcSets
        {
            get { return GlobalTranslation.Instance.Tx("WPK-Set bearbeiten",600,184); }
        }

        /// <summary>
        /// "Name des WPK-Sets"
        /// </summary>
        public static String EditFpcSetName
        {
            get { return GlobalTranslation.Instance.Tx("Name des WPK-Sets",600,185); }
        }

        /// <summary>
        /// "Bestätigen"
        /// </summary>
        public static String Confirm
        {
            get { return GlobalTranslation.Instance.Tx("Bestätigen",600,186); }
        }

        /// <summary>
        /// Es wurden nicht alle benötigten Felder ausgefüllt
        /// </summary>
        public static String RequiredFieldsFilled
        {
            get { return GlobalTranslation.Instance.Tx("Es wurden nicht alle notwendigen Felder ausgefüllt.",600,187); }
        }

        /// <summary>
        /// "Deutsch"
        /// </summary>
        public static String GermanLanguage
        {
            get { return GlobalTranslation.Instance.Tx("Deutsch",600,188); }
        }

        /// <summary>
        /// "Englisch"
        /// </summary>
        public static String EnglishLanguage
        {
            get { return GlobalTranslation.Instance.Tx("Englisch",600,189); }
        }

        /// <summary>
        /// "Globale Einstellungen"
        /// </summary>
        public static String GlobalSettings
        {
            get { return GlobalTranslation.Instance.Tx("Globale Einstellungen",600,190); }
        }

        /// <summary>
        /// "Lagertypen"
        /// </summary>
        public static String StoreTypes
        {
            get { return GlobalTranslation.Instance.Tx("Lagertypen",600,191); }
        }

        /// <summary>
        /// "Rahmenweise zusammenfassen"
        /// </summary>
        public static String KeepTogetherPerPosition
        {
            get { return GlobalTranslation.Instance.Tx("Rahmenweise zusammenfassen",600,192); }
        }

        /// <summary>
        /// "Einstellungen"
        /// </summary>
        public static String Settings
        {
            get { return GlobalTranslation.Instance.Tx("Einstellungen",600,193); }
        }

        /// <summary>
        /// "Terminals"
        /// </summary>
        public static String Terminals
        {
            get { return GlobalTranslation.Instance.Tx("Terminals",600,194); }
        }


        /// <summary>
        /// "Benutzer"
        /// </summary>
        public static String User
        {
            get { return GlobalTranslation.Instance.Tx("Benutzer",600,195); }
        }

        /// <summary>
        /// "Version"
        /// </summary>
        public static String Version
        {
            get { return GlobalTranslation.Instance.Tx("Version",600,196); }
        }

        /// <summary>
        /// "Spracheinstellungen"
        /// </summary>
        public static String LanguageSettings
        {
            get { return GlobalTranslation.Instance.Tx("Spracheinstellungen",600,197); }
        }

        /// <summary>
        /// "Anzeigesprache der Info-Server Komponenten"
        /// </summary>
        public static String DisplayableLanguageOfInfoServer
        {
            get { return GlobalTranslation.Instance.Tx("Anzeigesprache der Info-Server Komponenten. (Änderung wird mit Neustart aktiv)",600,198); }
        }

        /// <summary>
        /// "Lagertypen bearbeiten"
        /// </summary>
        public static String EditStoreType
        {
            get { return GlobalTranslation.Instance.Tx("Lagertypen bearbeiten",600,199); }
        }

        /// <summary>
        /// "Name des Lagertyps"
        /// </summary>
        public static String StoreTypeName
        {
            get { return GlobalTranslation.Instance.Tx("Name des Lagertyps",600,200); }
        }

        /// <summary>
        /// "Einlagerungsstrategie"
        /// </summary>
        public static String StorageStrategy
        {
            get { return GlobalTranslation.Instance.Tx("Einlagerungsmethode",600,201); }
        }

        /// <summary>
        /// "Lager bearbeiten"
        /// </summary>
        public static String EditStore
        {
            get { return GlobalTranslation.Instance.Tx("Lager bearbeiten",600,202); }
        }

        /// <summary>
        /// "Name des Lagers"
        /// </summary>
        public static String StoreName
        {
            get { return GlobalTranslation.Instance.Tx("Name des Lagers",600,203); }
        }

        /// <summary>
        /// "Auswahl des Lagertyps zu dem das Lager zugeordnet werden soll"
        /// </summary>
        public static String StoreTypeSelectionForStore
        {
            get { return GlobalTranslation.Instance.Tx("Auswahl des Lagertyps",600,204); }
        }


        /// <summary>
        /// "Auswahl der Facheinlagerungsstrategie"
        /// </summary>
        public static String ShelfStorageStrategySelectionForStore
        {
            get { return GlobalTranslation.Instance.Tx("Auswahl der Facheinlagerungsstrategie",600,315); }
        }

        /// <summary>
        /// "Benötigter Platz zwischen eingelagerten Elementen in MM"
        /// </summary>
        public static String StoreShelfMinspacing
        {
            get { return GlobalTranslation.Instance.Tx("Benötigter Platz zwischen eingelagerten Elementen in MM",600,316); }
        }

        /// <summary>
        /// "Mobiles Lager"
        /// </summary>
        public static String MobileStore
        {
            get { return GlobalTranslation.Instance.Tx("Mobiles Lager",600,205); }
        }

        /// <summary>
        /// "Anzahl der Fächer im Lager"
        /// </summary>
        public static String ShelfCount
        {
            get { return GlobalTranslation.Instance.Tx("Anzahl der Fächer im Lager",600,206);; }
        }

        /// <summary>
        /// "Fachkapazität pro Fach"
        /// </summary>
        public static String CapacityPerShelf
        {
            get { return GlobalTranslation.Instance.Tx("Fachkapazität",600,207); }
        }

        /// <summary>
        /// "Maximale Fachtiefe in mm"
        /// </summary>
        public static String ShelfDepthInMM
        {
            get { return GlobalTranslation.Instance.Tx("Maximale Lagerfachtiefe in mm",600,208); }
        }

        /// <summary>
        /// "Maximale Fachhöhe in mm"
        /// </summary>
        public static String ShelfHeightInMM
        {
            get { return GlobalTranslation.Instance.Tx("Maximale Lagerfachhöhe in mm",600,209); }
        }

        /// <summary>
        /// "Maximale FachBreite in mm"
        /// </summary>
        public static String ShelfWidthInMM
        {
            get { return GlobalTranslation.Instance.Tx("Maximale Lagerfachbreite in mm",600,317); }
        }

        /// <summary>
        /// "Breite des Transportbandes in mm"
        /// </summary>
        public static String TransportCartWidthInMM
        {
            get { return GlobalTranslation.Instance.Tx("Breite des Transportbandes in mm",600,405); }
        }


        /// <summary>
        /// "Terminal-Einstellungen"
        /// </summary>
        public static String TerminalSettings
        {
            get { return GlobalTranslation.Instance.Tx("Terminal-Einstellungen",600,210); }
        }

        /// <summary>
        /// "Pincode zum Beenden des Terminals"
        /// </summary>
        public static String PincodeForExitingTerminal
        {
            get { return GlobalTranslation.Instance.Tx("Pincode zum Beenden des Terminal-Programmes",600,211); }
        }

        /// <summary>
        /// "Verzeichnis in dem sich die neue Version des Terminal-Clients befindet"
        /// </summary>
        public static String DirectoryOfNewClientVersion
        {
            get { return GlobalTranslation.Instance.Tx("Verzeichnis, in dem sich die neue Programm-Version befindet",600,212); }
        }

        /// <summary>
        /// "Präfix welcher als Indizierung für die Personal-Barcodes dient"
        /// </summary>
        public static String UserBarcodePraefix
        {
            get { return GlobalTranslation.Instance.Tx("Firmenkürzel für die Verwendung vor dem Mitarbeiter-Barcode",600,213); }
        }

        public static String DocumentViewerBarcodePraefix
        {
            get { return GlobalTranslation.Instance.Tx("Praefix für die Verwendung des DocumentViewer-Barcode",600,406); }
        }

        /// <summary>
        /// "Herunterfahren bei Schließen des Terminals"
        /// </summary>
        public static String ExitOnTerminalClose
        {
            get { return GlobalTranslation.Instance.Tx("Herunterfahren bei Schließen des Terminals",600,214); }
        }

        /// <summary>
        /// "Terminal bearbeiten"
        /// </summary>
        public static String EditTerminal
        {
            get { return GlobalTranslation.Instance.Tx("Terminal bearbeiten",600,215); }
        }
        
        /// <summary>
        /// "Name des Terminals"
        /// </summary>
        public static String TerminalName
        {
            get { return GlobalTranslation.Instance.Tx("Name des Terminals",600,216); }
        }

        /// <summary>
        /// "Benutzer bearbeiten"
        /// </summary>
        public static String EditUser
        {
            get { return GlobalTranslation.Instance.Tx("Benutzer bearbeiten",600,217); }
        }

        /// <summary>
        /// "Benutzername"
        /// </summary>
        public static String UserName
        {
            get { return GlobalTranslation.Instance.Tx("Benutzername",600,218); }
        }

        /// <summary>
        /// "Die Personalnummer wird als Druck für den Barcode verwendet und dient zur Anmeldung an dem Terminal"
        /// </summary>
        public static String UserPersonalNumber
        {
            get { return GlobalTranslation.Instance.Tx("Die Personalnummer zur Erzeugung eines Barcodes für die Anmeldung am Arbeitsplatz",600,219); }
        }

        /// <summary>
        /// "Barcode drucken"
        /// </summary>
        public static String PrintBarcode
        {
            get { return GlobalTranslation.Instance.Tx("Barcode drucken",600,220); }
        }

        /// <summary>
        /// "Arbeitsplatz bearbeiten"
        /// </summary>
        public static String EditWorkStation
        {
            get { return GlobalTranslation.Instance.Tx("Arbeitsplatz bearbeiten",600,221); }
        }

        /// <summary>
        /// "Name des Arbeitsplatzes"
        /// </summary>
        public static String WorkStationName
        {
            get { return GlobalTranslation.Instance.Tx("Name des Arbeitsplatzes",600,222); }
        }

        /// <summary>
        /// "Die Kurzbezeichnung des Arbeitsplatzes bezeichnet den Namen der im Terminal-Client angezeigt wird"
        /// </summary>
        public static String WorkStationShortName
        {
            get { return GlobalTranslation.Instance.Tx("Kurzbezeichnung des Arbeitsplatzes",600,223); }
        }

        /// <summary>
        /// "Der Einlieferungspunkt für ein evtl. vorhandenes automatisches Lagersystem. Hier werden die Teile einer Workstation eingelagert."
        /// </summary>
        public static String WorkStationStoragePoint
        {
            get { return GlobalTranslation.Instance.Tx("Einlieferungspunkt für automatisches Lagersystem",600,318); }
        }

        /// <summary>
        /// "Der Ausgabepunkt für ein evtl. vorhandenes automatisches Lagersystem. Hier werden die Teile aus dem Lager der Workstation zur Verfügung gestellt."
        /// </summary>
        public static String WorkStationDeliveryPoint
        {
            get { return GlobalTranslation.Instance.Tx("Ausgabepunkt des automatischen Lagersystems",600,319); }
        }

        /// <summary>
        /// "Auswahl des Druckers über den die Barcodes gedruckt werden sollen"
        /// </summary>
        public static String PrinterSelection
        {
            get { return GlobalTranslation.Instance.Tx("Auswahl des Barcodedruckers",600,224); }
        }

        /// <summary>
        /// "Substeps des Arbeitsplatzes automatisch aktivieren"
        /// </summary>
        public static String ActivateSubStepsAutomatically
        {
            get { return GlobalTranslation.Instance.Tx("Teilarbeitsschritte am Arbeitsplatz automatisch aktivieren",600,225); }
        }

        /// <summary>
        /// "Zugehörigkeit zum Terminal"
        /// </summary>
        public static String TerminalOfWorkStation
        {
            get { return GlobalTranslation.Instance.Tx("Zugehörig zum Terminal",600,226); }
        }

        /// <summary>
        /// "Einlagerung verzögern"
        /// </summary>
        public static String StorageDelay
        {
            get { return GlobalTranslation.Instance.Tx("Einlagerung verzögern",600,227); }
        }

        /// <summary>
        /// "Arbeitszeit innerhalb des Terminal-Clients anzeigen"
        /// </summary>
        public static String ShowWorkingTime
        {
            get { return GlobalTranslation.Instance.Tx("Arbeitszeit am Arbeitsplatz anzeigen",600,228); }
        }

        /// <summary>
        /// "Alternativen an der Maschine anzeigen"
        /// </summary>
        public static String ShowAlternatives
        {
            get { return GlobalTranslation.Instance.Tx("Alternativen an der Maschine anzeigen",600,229); }
        }

        /// <summary>
        /// "Automatisches Starten des nächsten Teilschrittes deaktivieren"
        /// </summary>
        public static String AutomaticallyStartOfNextSubStep
        {
            get { return GlobalTranslation.Instance.Tx("Direktes Fertigstellen der Teilschritte durch Barcodescan",600,320); }
        }

        /// <summary>
        /// "Katalogue bearbeiten"
        /// </summary>
        public static String EditCatalogue
        {
            get { return GlobalTranslation.Instance.Tx("Kataloge bearbeiten",600,231); }
        }

        /// <summary>
        /// "Eingabe des Herstellerkürzels. (Maximal drei Zeichen)"
        /// </summary>
        public static String CatalogueSupplier
        {
            get { return GlobalTranslation.Instance.Tx("Herstellerkürzel (Maximal drei Zeichen)",600,232); }
        }

        /// <summary>
        /// "Eingabe der Website welche zu dem Katalog des eingegebenen Herstellerkürzels führt"
        /// </summary>
        public static String CatalogueLink
        {
            get { return GlobalTranslation.Instance.Tx("Eingabe der Internetadresse zu dem Katalog dieses Herstellers",600,233); }
        }

        /// <summary>
        /// "Kataloge"
        /// </summary>
        public static String Catalogue
        {
            get { return GlobalTranslation.Instance.Tx("Kataloge",600,234); }
        }

        /// <summary>
        /// "Hinzufügen"
        /// </summary>
        public static String Add
        {
            get { return GlobalTranslation.Instance.Tx("Hinzufügen",600,235); }
        }

        /// <summary>
        /// "Allgemeine Einstellungen"
        /// </summary>
        public static String GeneralSettings
        {
            get { return GlobalTranslation.Instance.Tx("Allgemeine Einstellungen",600,271); }
        }

        /// <summary>
        /// "Service-Code anfordern"
        /// </summary>
        public static String ServiceCode
        {
            get { return GlobalTranslation.Instance.Tx("Service-Code anfordern",600,272); }
        }

        /// <summary>
        /// "Das Anfordern Ihres Service-Codes hängt von Ihrer Internetverbindung ab und kann mehrere Sekunden dauern"
        /// </summary>
        public static String ServiceCodeInformation
        {
            get { return GlobalTranslation.Instance.Tx("Das Anfordern Ihres Service-Codes hängt von Ihrer Internetverbindung ab und kann mehrere Sekunden dauern.",600,273); }
        }

        /// <summary>
        /// "Versionsnummer des InfoServer-Service"
        /// </summary>
        public static String ServiceVersion
        {
            get { return GlobalTranslation.Instance.Tx("Versionsnummer des InfoServer-Service",600,274); }
        }

        /// <summary>
        /// "Vorgängerdialog auch bei Aktivieren ohne Barcode-Scanner anzeigen"
        /// </summary>
        public static String ShowPredecessors
        {
            get { return GlobalTranslation.Instance.Tx("Vorgängerdialog auch bei Aktivieren ohne Barcode-Scanner anzeigen",600,278); }
        }

        /// <summary>
        /// "Aktiver Benutzer"
        /// </summary>
        public static String InactiveUser
        {
            get { return GlobalTranslation.Instance.Tx("Aktiver Benutzer",600,280); }
        }

        /// <summary>
        /// "Herstellername"
        /// </summary>
        public static String CatalogueSupplierName
        {
            get { return GlobalTranslation.Instance.Tx("Herstellername",600,285); }
        }

        /// <summary>
        /// "Alternative Folgearbeitsplätze anzeigen"
        /// </summary>
        public static String ShowAlternativeSuccessorWorkStations
        {
            get { return GlobalTranslation.Instance.Tx("Alternative Folgearbeitsplätze anzeigen",600,286); }
        }

        /// <summary>
        /// "Dokumente können vor Start des Produktionsschrittes eingesehen werden"
        /// </summary>
        public static String DocumentPreview
        {
            get { return GlobalTranslation.Instance.Tx("Dokumente können vor Start des Produktionsschrittes eingesehen werden",600,287); }
        }

        /// <summary>
        /// "Zeilenabstand vergrößern (verbesserte Touchbedienung)"
        /// </summary>
        public static String LineSpace
        {
            get { return GlobalTranslation.Instance.Tx("Zeilenabstand vergrößern (verbesserte Touchbedienung)",600,311); }
        }

        /// <summary>
        /// "Profilbilder bei Produktionsschritten anzeigen"
        /// </summary>
        public static String ShowProductionStepPictures
        {
            get { return GlobalTranslation.Instance.Tx("Profilbilder bei Produktionsschritten anzeigen",600,291); }
        }

        /// <summary>
        /// "Benötigte Teile müssen gescanned werden"
        /// </summary>
        public static String PredecessorsNeedsToBeScanned
        {
            get { return GlobalTranslation.Instance.Tx("Benötigte Teile müssen gescanned werden",600,292); }
        }

        /// <summary>
        /// "Benötigte Teile müssen gescanned werden"
        /// </summary>
        public static String SwapIdenticalSubSteps
        {
            get { return GlobalTranslation.Instance.Tx("Alternative Bauteile zusammenfügen",600,293); }
        }

        /// <summary>
        /// "Hinweis"
        /// </summary>
        public static String Tip
        {
            get { return GlobalTranslation.Instance.Tx("Hinweis",600,295); }
        }

        public static String AutomaticStore
        {
            get { return GlobalTranslation.Instance.Tx("Automatisches Lager",600,321); }
        }

        /// <summary>
        /// "Präsentationsmodus aktivieren"
        /// </summary>
        public static String ActivatePresentationMode
        {
            get { return GlobalTranslation.Instance.Tx("Präsentationsmodus aktivieren",600,322); }
        }

        /// <summary>
        /// "Präsentationsmodus"
        /// </summary>
        public static String PresentationMode
        {
            get { return GlobalTranslation.Instance.Tx("Präsentationsmodus",600,323); }
        }

        /// <summary>
        /// "Name des Präsentationsszenarios"
        /// </summary>
        public static String PresentationDumpName
        {
            get { return GlobalTranslation.Instance.Tx("Name des Präsentationsszenarios",600,324); }
        }

        /// <summary>
        /// "Werkstattplan"
        /// </summary>
        public static String PresentationHtmlInformation
        {
            get { return GlobalTranslation.Instance.Tx("Werkstattplan",600,325); }
        }

        /// <summary>
        /// "Werkstattplan"
        /// </summary>
        public static String LoadPresentationHtmlInformation
        {
            get { return GlobalTranslation.Instance.Tx("Werkstattplan laden",600,326); }
        }

        /// <summary>
        /// "Szenario"
        /// </summary>
        public static String PresentationSzenarioInformation
        {
            get { return GlobalTranslation.Instance.Tx("Szenario",600,327); }
        }

        /// <summary>
        /// "Szenario"
        /// </summary>
        public static String LoadPresentationSzenario
        {
            get { return GlobalTranslation.Instance.Tx("Szenario laden",600,328); }
        }

        /// <summary>
        /// "Host"
        /// </summary>
        public static String FtpHostName
        {
            get { return GlobalTranslation.Instance.Tx("Host",600,329); }
        }

        /// <summary>
        /// "Passwort"
        /// </summary>
        public static String Password
        {
            get { return GlobalTranslation.Instance.Tx("Passwort",600,330); }
        }

        /// <summary>
        /// "FTP-Einstellungen"
        /// </summary>
        public static String FtpSettings
        {
            get { return GlobalTranslation.Instance.Tx("FTP-Einstellungen",600,331); }
        }

        /// <summary>
        /// "Port"
        /// </summary>
        public static String FtpPort
        {
            get { return GlobalTranslation.Instance.Tx("Port",600,332); }
		}
			
		/// <summary>
        /// "Niederländisch"
        /// </summary>
        public static String NetherlandLanguage
        {
            get { return GlobalTranslation.Instance.Tx("Niederländisch",600,333); }
        }

        /// <summary>
        /// "Französisch
        /// </summary>
        public static String FrenchLanguage
        {
            get { return GlobalTranslation.Instance.Tx("Französisch",600,360); }
        }

        /// <summary>
        /// "Niederländisch"
        /// </summary>
        public static String BlockUnimportantErrors
        {
            get { return GlobalTranslation.Instance.Tx("Diverse Meldungen unterdrücken",600,334); }
        }

        /// <summary>
        /// "Löschen"
        /// </summary>
        public static String Delete
        {
            get { return GlobalTranslation.Instance.Tx("Löschen",600,361); }
        }

        /// Benutzeranmeldung nur über Barcode
        /// </summary>
        public static String OnlyBarcodeLogin
        {
            get { return GlobalTranslation.Instance.Tx("Benutzeranmeldung nur über Barcode",600,398); }
        }

        /// <summary>
        /// "Update"
        /// </summary>
        public static String CheckForUpdate
        {
            get { return GlobalTranslation.Instance.Tx("Update",600,362); }
        }

        /// <summary>
        /// "Das Update wird durchgeführt. Bitte Haben sie einen Moment Geduld. Der Werkstattleiter wird nach Herunterladen des Updates neugestaretet."
        /// </summary>
        public static String UpdateMessage
        {
            get { return GlobalTranslation.Instance.Tx("Das Update wird durchgeführt. Bitte Haben sie einen Moment Geduld.",600,363); }
        }

        /// <summary>
        /// "Name des DocumentViewers"
        /// </summary>
        public static String DocumentViewerName
        {
            get { return GlobalTranslation.Instance.Tx("Name des DocumentViewers",600,407); }
        }

        /// <summary>
        /// "Barcode"
        /// </summary>
        public static String Barcode
        {
            get { return GlobalTranslation.Instance.Tx("Barcode",600,408); }
        }
        /// <summary>
        /// "Maschinentyp"
        /// </summary>
        public static String MachineType
        {
            get { return GlobalTranslation.Instance.Tx("Maschinentyp",600,409); }
        }

        /// <summary>
        /// "Pin zur Verwendung des Lagerterminals"
        /// </summary>
        public static String StorageTerminalPin
        {
            get { return GlobalTranslation.Instance.Tx("Pin zur Verwendung des Lagerterminals",600,410); }
        }

        /// <summary>
        /// "Zugehörig zum Arbeitsplatz"
        /// </summary>
        public static String WorkstationOfDocumentViewer
        {
            get { return GlobalTranslation.Instance.Tx("Zugehörig zum Arbeitsplatz",600,411); }
        }

        /// <summary>
        /// "Automatische Aktualisierung"
        /// </summary>
        public static String AutoRefresh
        {
            get { return GlobalTranslation.Instance.Tx("Automatische Aktualisierung",600,412); }
        }

        /// <summary>
        /// "DocumentViewer"
        /// </summary>
        public static String DocumentViewer
        {
            get { return GlobalTranslation.Instance.Tx("DocumentViewer",600,413); }
        }


        /// <summary>
        /// "Name der Maschinenschnitstelle"
        /// </summary>
        public static String MachineInterfaceName
        {
            get { return GlobalTranslation.Instance.Tx("Name der Maschinenschnittstelle",600,441); }
        }

        /// <summary>
        /// "EndPointAdress"
        /// </summary>
        public static String EndpointAdress
        {
            get { return GlobalTranslation.Instance.Tx("Endpunkt Adresse",600,442); }
        }
      
        /// <summary>
        /// "MachineInterface"
        /// </summary>
        public static String UsedMachineInterface
        {
            get { return GlobalTranslation.Instance.Tx("Verwendete Maschinenschnittstelle",600,443); }
        }

        public static String MachineInterfaces
        {
            get { return GlobalTranslation.Instance.Tx("Maschinenschnittstellen",600,444); }
        }

        public static String AdressalreadyInUse
        {
            get {return GlobalTranslation.Instance.Tx("Es existiert bereits eine Schnittstelle mit dieser Adresse",600,445);}
        }

        public static String Machines
        {
            get { return GlobalTranslation.Instance.Tx("Maschinen",600,446); }
        }

        public static String MachineInterfaceType
        {
            get { return GlobalTranslation.Instance.Tx("InterfaceTyp",600,447); }
        }
        public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Es existiert bereits eine Schnittstelle von diesem Typen",600,455); }
        } 
		
		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Test",600,490); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Test",600,490); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,532); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,533); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,534); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,535); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,536); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,537); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,538); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,539); }
        }

		public static String TypealreadyInUse
        {
            get { return GlobalTranslation.Instance.Tx("Chris ist der beste",600,540); }
        }

		

    }
}

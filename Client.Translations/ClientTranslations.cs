﻿using System;
using Orgadata.Framework.Translation;
using Shared.Translations;

namespace Client.Translations
{
    public class ClientTranslations : SharedTranslations
    {
        /// <summary>
        /// "Fertigstellen"
        /// </summary>
        public static String FinishContent
        {
            get { return GlobalTranslation.Instance.Tx("Fertigstellen", 600, 3); }
        }

        /// <summary>
        /// "Pausieren"
        /// </summary>
        public static String PauseContent
        {
            get { return GlobalTranslation.Instance.Tx("Pausieren", 600, 4); }    
        }

        /// <summary>
        /// "Filter"
        /// </summary>
        public static String FilterContent
        {
            get { return GlobalTranslation.Instance.Tx("Filter",600,170); }
        }

        /// <summary>
        /// "Projekte"
        /// </summary>
        public static String BatchContent
        {
            get { return GlobalTranslation.Instance.Tx("Projekte", 600, 5); }
        }

        /// <summary>
        /// "Suche"
        /// </summary>
        public static String SearchContent
        {
            get { return GlobalTranslation.Instance.Tx("Suche", 600, 6); }
        }

        /// <summary>
        /// "Dokumente"
        /// </summary>
        public static String DocumentTitle
        {
            get { return GlobalTranslation.Instance.Tx("Dokumente", 600, 16); }
        }
        
        /// <summary>
        /// "Produktkatalog"
        /// </summary>
        public static String CatalogueTitle
        {
            get { return GlobalTranslation.Instance.Tx("Produktkatalog", 600, 10); }
        }

        /// <summary>
        /// "Artikelnummer"
        /// </summary>
        public static String CataloguePlaceholder
        {
            get { return GlobalTranslation.Instance.Tx("Artikelnummer", 600, 11); }
        }

        /// <summary>
        /// "Suchen"
        /// </summary>
        public static String CatalogueSearchContent
        {
            get { return GlobalTranslation.Instance.Tx("Suchen", 600, 12); }
        }

        /// <summary>
        /// "Alles aktivieren"
        /// </summary>
        public static String DebugActivateAllContent
        {
            get { return GlobalTranslation.Instance.Tx("Alles aktivieren",600,104); }
        }

        /// <summary>
        /// "Alles anfertigen"
        /// </summary>
        public static String DebugFinishAllContent
        {
            get { return GlobalTranslation.Instance.Tx("Alles anfertigen",600,105); }
        }

        /// <summary>
        /// "Aktivieren und Fertigen"
        /// </summary>
        public static String DebugActivateAndFinish
        {
            get { return GlobalTranslation.Instance.Tx("Aktivieren und Fertigen",600,106); }
        }

        /// <summary>
        /// "Barcode"
        /// </summary>
        public static String DebugBarcodePlaceholder
        {
            get { return GlobalTranslation.Instance.Tx("Barcode",600,107); }
        }

        /// <summary>
        /// "Scannereingaben prüfen"
        /// </summary>
        public static String DebugScannerCheck
        {
            get { return GlobalTranslation.Instance.Tx("Scannereingaben prüfen",600,133); }
        }

        /// <summary>
        /// "Vorgänger anzeigen"
        /// </summary>
        public static String OpenPredecessorDialog
        {
            get { return GlobalTranslation.Instance.Tx("Vorgänger anzeigen",600,138); }
        }

        /// <summary>
        /// "Freie Eingabe"
        /// </summary>
        public static String FreeInputPlaceholder
        {
            get { return GlobalTranslation.Instance.Tx("Freie Eingabe", 600, 33); }
        }

        /// <summary>
        /// "Geben Sie den Fehler an"
        /// </summary>
        public static String FpcErrorTitle
        {
            get { return GlobalTranslation.Instance.Tx("Geben Sie den Fehler an", 600, 34); }    
        }

        /// <summary>
        /// "Terminal-ID löschen"
        /// </summary>
        public static String DeleteTerminalIdTitle
        {
            get { return GlobalTranslation.Instance.Tx("Terminal-ID löschen",600,134); }
        }

        /// <summary>
        /// "Soll"
        /// </summary>
        public static String GlazingBeadDesiredTitle
        {
            get { return GlobalTranslation.Instance.Tx("Soll", 600, 19); }
        }

        /// <summary>
        /// "Ist"
        /// </summary>
        public static String GlazingBeadActualTitle
        {
            get { return GlobalTranslation.Instance.Tx("Ist", 600, 20); }
        }

        /// <summary>
        /// "Ok"
        /// </summary>
        public static String Ok
        {
            get { return GlobalTranslation.Instance.Tx("Ok",600,108); }
        }

        /// <summary>
        /// "Abbrechen"
        /// </summary>
        public static String AbortContent
        {
            get { return GlobalTranslation.Instance.Tx("Abbrechen", 600, 42); }
        }

        /// <summary>
        /// "Schließen"
        /// </summary>
        public static String CloseContent
        {
            get { return GlobalTranslation.Instance.Tx("Schließen", 600, 7); }
        }

        /// <summary>
        /// "Benötigte Bauteile"
        /// </summary>
        public static String PredecessorTitle
        {
            get { return GlobalTranslation.Instance.Tx("Benötigte Bauteile", 600, 24); }
        }

        /// <summary>
        /// "Zugehörige Barcodes"
        /// </summary>
        public static String AttachedBarcodes
        {
            get { return GlobalTranslation.Instance.Tx("Zugehörige Barcodes",600,171); }
        }

        /// <summary>
        /// "Bereits angemeldete Benutzer"
        /// </summary>
        public static String LoggedUserTitle
        {
            get { return GlobalTranslation.Instance.Tx("Bereits angemeldete Benutzer",600,269); }
        }

        /// <summary>
        /// "Alle Benutzer"
        /// </summary>
        public static String AllUserTitle
        {
            get { return GlobalTranslation.Instance.Tx("Alle Benutzer",600,110); }
        }

        /// <summary>
        /// "Anmelden"
        /// </summary>
        public static String LoginContent
        {
            get { return GlobalTranslation.Instance.Tx("Anmelden",600,111); }
        }

        /// <summary>
        /// "Abmelden"
        /// </summary>
        public static String LogoutContent
        {
            get { return GlobalTranslation.Instance.Tx("Abmelden",600,112); }
        }

        /// <summary>
        /// "Bitte geben Sie den hinterlegten Pin zum Beenden des Programmes ein."
        /// </summary>
        public static String PinInputTitle
        {
            get { return GlobalTranslation.Instance.Tx("Bitte geben Sie den hinterlegten Pin zum Beenden des Programmes ein.",600,113); }
        }

        /// <summary>
        /// "Pin-Code"
        /// </summary>
        public static String PinInputPlaceholder
        {
            get { return GlobalTranslation.Instance.Tx("Pin-Code",600,114); }
        }

        /// <summary>
        /// "Freigeben"
        /// </summary>
        public static String StoreReleaseContent
        {
            get { return GlobalTranslation.Instance.Tx("Freigeben",600,116); }   
        }

        /// <summary>
        /// "Länge"
        /// </summary>
        public static String LengthTitle
        {
            get { return GlobalTranslation.Instance.Tx("Länge",600,118); }
        }

        /// <summary>
        /// "Fach"
        /// </summary>
        public static String GroupTitle
        {
            get { return GlobalTranslation.Instance.Tx("Fach",600,119); }
        }

        /// <summary>
        /// "Barcode"
        /// </summary>
        public static String BarcodeTitle
        {
            get { return GlobalTranslation.Instance.Tx("Barcode",600,120); }    
        }

        /// <summary>
        /// "Nachfolger"
        /// </summary>
        public static String NextWorkStationTitle
        {
            get { return GlobalTranslation.Instance.Tx("Nachfolger",600,121); }
        }

        /// <summary>
        /// "mm"
        /// </summary>
        public static String UnitMM
        {
            get { return GlobalTranslation.Instance.Tx("mm",600,122); }
        }

        /// <summary>
        /// "Bitte wählen sie einen Nutzer aus."
        /// </summary>
        public static String UserSelectionTitle
        {
            get { return GlobalTranslation.Instance.Tx("Bitte wählen sie einen Nutzer aus.",600,135); }
        }

        /// <summary>
        /// "Profile"
        /// </summary>
        public static String ProfileTitle
        {
            get { return GlobalTranslation.Instance.Tx("Profile",600,172); }   
        }

        /// <summary>
        /// "Farben"
        /// </summary>
        public static String ColorTitle
        {
            get { return GlobalTranslation.Instance.Tx("Farben",600,173); }
        }

        /// <summary>
        /// "Bitte scannen Sie einen Barcode"
        /// </summary>
        public static String InformationDialog
        {
            get { return GlobalTranslation.Instance.Tx("Bitte scannen Sie einen Barcode.",600,174); }    
        }

        /// <summary>
        /// "Profilnummer"
        /// </summary>
        public static String ProfileNumber
        {
            get { return GlobalTranslation.Instance.Tx("Profilnummer",600,175); }
        }

        /// <summary>
        /// "Profilbeschreibung"
        /// </summary>
        public static String ProfileDescription
        {
            get { return GlobalTranslation.Instance.Tx("Profilbeschreibung",600,176); }    
        }

        /// <summary>
        /// "Auftragsnummer"
        /// </summary>
        public static String JobNumber
        {
            get { return GlobalTranslation.Instance.Tx("Auftragsnummer",600,177); }
        }

        /// <summary>
        /// "Positionsnummer"
        /// </summary>
        public static String PositionNumber
        {
            get { return GlobalTranslation.Instance.Tx("Positionsnummer",600,178); }
        }

        /// <summary>
        /// "Vorgesehen an Arbeitsplatz"
        /// </summary>
        public static String ProcessableOnWorkStation
        {
            get { return GlobalTranslation.Instance.Tx("Vorgesehen an Arbeitsplatz",600,401); }    
        }

        /// <summary>
        /// "Es wurden keine Informationen zu dem Barcode gefunden"
        /// </summary>
        public static String NoInformationFound
        {
            get { return GlobalTranslation.Instance.Tx("Es wurden keine Informationen zu dem Barcode gefunden.",600,180); }    
        }

        /// <summary>
        /// "Bitte wählen Sie einen der Kataloge aus"
        /// </summary>
        public static String CatalogueSelection
        {
            get { return GlobalTranslation.Instance.Tx("Bitte wählen Sie einen Katalog aus.",600,270); }
        }

        /// <summary>
        /// "Bitte wählen Sie einen der Kataloge aus"
        /// </summary>
        public static String BarcodeDialogTitle
        {
            get { return GlobalTranslation.Instance.Tx("Vorgänger-Barcodes",600,288); }
        }

        /// <summary>
        /// "Es wurde kein zugehöriger Katalog gefunden."
        /// </summary>
        public static String NoCatalogueFound
        {
            get { return GlobalTranslation.Instance.Tx("Es wurde kein zugehöriger Katalog gefunden.", 600, 143); }
        }

        /// <summary>
        /// "Szenarien herunterladen"
        /// </summary>
        public static String DownloadScenario
        {
            get { return GlobalTranslation.Instance.Tx("Szenarien herunterladen",600,342); }
        }

        /// <summary>
        /// Pos.
        /// </summary>
        public static String Pos
        {
            get { return GlobalTranslation.Instance.Tx("Pos.",600,343); }
        }

        /// <summary>
        /// Lage
        /// </summary>
        public static String Position
        {
            get { return GlobalTranslation.Instance.Tx("Lage",600,344); }
        }

        /// <summary>
        /// Sachbearbeiter
        /// </summary>
        public static String PersonInCharge
        {
            get { return GlobalTranslation.Instance.Tx("Sachbearbeiter",600,402); }
        }

        /// <summary>
        /// Los
        /// </summary>
        public static String Phase
        {
            get { return GlobalTranslation.Instance.Tx("Los",600,354); }
        }

        /// <summary>
        /// Projekt
        /// </summary>
        public static String Batch
        {
            get { return GlobalTranslation.Instance.Tx("Projekt",600,355); }
        }


        public static String EditFilter
        {
            get { return GlobalTranslation.Instance.Tx("Filter editieren",600,369); }
		}
        public static String NoFurtherInfo
        {
            get { return GlobalTranslation.Instance.Tx("Es sind keine weiteren Informationen verfügbar",600,370); }

        }

        public static String UserSelectionTitleForFilter
        {
            get { return GlobalTranslation.Instance.Tx("Filterauswahl für angemeldete Benutzer.",600,371); }
        }

        public static String UseFilter
        {
            get { return GlobalTranslation.Instance.Tx("Filter anwenden",600,372); }
        }

        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 491); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 492); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 493); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 494); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 495); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 496); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 497); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 498); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 499); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 500); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 501); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 502); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 503); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 504); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 505); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 506); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 507); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 508); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 509); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 510); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 511); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 512); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 513); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 514); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 515); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 516); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 517); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 518); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 519); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 520); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 521); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 522); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 523); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 524); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 525); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 526); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 527); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 6600529, 528); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 542); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 543); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 544); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 545); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 546); }
        }
        public static String ProductionName
        {
            get { return GlobalTranslation.Instance.Tx("Fertigungsname", 600, 547); }
        }
    }
}
